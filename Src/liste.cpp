#include "liste.hpp"

#include <iostream>
#include <cassert>

Liste::Liste() {
  tailleListe = 0;
  teteListe = nullptr;
  queueListe = nullptr;
}

Liste::Liste(const Liste& autre) {
  Cellule* temp = autre.teteListe;
  while(temp != nullptr){
    ajouter_en_queue(temp->valeur);
    temp = temp->suivante;
  }
}

Liste& Liste::operator=(const Liste& autre) {
  Cellule* temp = autre.teteListe;
  while(temp != nullptr){
    ajouter_en_queue(temp->valeur);
    temp = temp->suivante;
  }
  return *this ;
}

Liste::~Liste() {
  tailleListe = 0;
  Cellule* temp = teteListe;
  while(temp != nullptr){
    supprimer_en_tete();
    temp = temp->suivante;
  }
}

void Liste::ajouter_en_tete(int valeur) {
  Cellule* temp = new Cellule();
  temp->valeur = valeur;
  if(teteListe == nullptr){
    teteListe = temp;
    queueListe = temp;
    temp = nullptr;
  }
  else{
    temp->suivante = teteListe;
    teteListe = temp;
  }
  tailleListe++;
}

void Liste::ajouter_en_queue(int valeur) {
    Cellule* temp = new Cellule();
  temp->valeur = valeur;
  if(teteListe == nullptr){
    teteListe = temp;
    queueListe = temp;
    temp = nullptr;
  }
  else{
    queueListe->suivante = temp;
    queueListe = temp;
  }
  tailleListe++;
}

void Liste::supprimer_en_tete() {
  Cellule* temp = new Cellule();
  temp = teteListe;
  teteListe = temp->suivante;
  delete temp;
  tailleListe--;
}

Cellule* Liste::tete() {
  return teteListe ;
}

const Cellule* Liste::tete() const {
  return teteListe ;
}

Cellule* Liste::queue() {
  return queueListe ;
}

const Cellule* Liste::queue() const {
  return queueListe ;
}

int Liste::taille() const {
  return tailleListe;
}

Cellule* Liste::recherche(int valeur) {
  Cellule* courante = teteListe;
  while (courante != NULL)  
  {  
      if (courante->valeur == valeur)  
          return courante;  
      courante = courante->suivante;  
  }
  return nullptr ;
}

const Cellule* Liste::recherche(int valeur) const {
  Cellule* courante = teteListe;
  while (courante != NULL)  
  {  
      if (courante->valeur == valeur)  
          return courante;  
      courante = courante->suivante;  
  }
  return nullptr ;
}

void Liste::afficher() const {
  
  Cellule* temp = teteListe;
  std::cout << "[ ";
  
  while(temp != nullptr){
    std::cout << temp->valeur << " ";
    temp = temp->suivante;
  }

  std::cout << " ]" << std::endl;
}

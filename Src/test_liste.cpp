#include "liste.hpp"

#include <iostream>
#include <cassert>

int main() {

  /* adaptez ce fichier selon votre structure */

  Liste l1 ;
  l1.ajouter_en_tete(10) ;
  l1.ajouter_en_tete(11) ;
  std::cout << "attendu : [ 11 10 ]" << std::endl ;
  l1.afficher() ; // [ 11 10 ]
  assert(l1.tete()->valeur == 11) ;
  assert(l1.queue()->valeur == 10) ;
  assert(l1.recherche(11)) ;
  assert(l1.recherche(10)) ;
  assert(!l1.recherche(12)) ;
  assert(l1.taille() == 2) ;
  
  Liste l2(l1) ;
  l2.ajouter_en_tete(20) ;
  std::cout << "attendu : [ 20 11 10 ]" << std::endl ;
  l2.afficher() ; // [ 20 11 10 ]
  assert(l2.tete()->valeur == 20) ;
  assert(l2.queue()->valeur == 10) ;
  assert(l2.recherche(20)) ;
  assert(l2.recherche(11)) ;
  assert(l2.recherche(10)) ;
  assert(!l2.recherche(21)) ;


  return 0 ;
}
